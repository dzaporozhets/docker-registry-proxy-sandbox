# docker-registry-proxy-sandbox

Running proxy registry locally on Ubuntu machine. For development purposes only 

start the registry: 

```
sh start_linux.sh
```

try if its working:

```
sudo docker pull localhost:5005/library/ruby:alpine
sudo docker pull localhost:5005/gitlab/gitlab-ce
```

stop and cleanup:

```
sh stop_rm_linux.sh
```
